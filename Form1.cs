﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace DansScriptMngr
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ShowUserControl("Doc");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ShowUserControl("Scripts");
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            ShowUserControl("AppLauncher");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ShowUserControl("ImgBuilder");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ShowUserControl("Contacts");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ShowUserControl("ComingSoon");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ShowUserControl("ComingSoon");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            ShowUserControl("ComingSoon");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            ShowUserControl("ComingSoon");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            ShowUserControl("ComingSoon");
        }

        public void ShowUserControl(string val)
        {
            UserControl uc = new UserControl();
            pnlMain.Controls.Clear();

            if (val == "Doc")
            {
                uc = new ctlDoc();
            }

            if (val == "Scripts")
            {
                uc = new ctlScripts();
            }

            if (val == "AppLauncher")
            {
                uc = new ctlAppLauncher();
            }

            if (val == "ImgBuilder")
            {
                uc = new ctlImgBuilder();
            }

            if (val == "Contacts")
            {
                uc = new ctlContacts();
            }

            if (val == "ComingSoon")
            {
                uc = new ctlComingSoon();
            }
            pnlMain.Controls.Add(uc);
        }

 

        
    }
}
