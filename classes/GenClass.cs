﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace DansScriptMngr.classes
{
    public class GenClass
    {
        #region Load TreeView

        /// <summary>
        /// 
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="path"></param>
        public static void ListDirectory(TreeView treeView, string path)
        {
            //clear out all existing nodes from the treeview
            treeView.Nodes.Clear();

            //get the directory info from the 'path' to add to the treeview
            var rootDirectoryInfo = new DirectoryInfo(path);

            //add the nodes to the treeview
            treeView.Nodes.Add(CreateDirectoryNode(rootDirectoryInfo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directoryInfo"></param>
        /// <returns></returns>
        public static TreeNode CreateDirectoryNode(DirectoryInfo directoryInfo)
        {
            //set the name of the root node
            var directoryNode = new TreeNode(directoryInfo.Name);

            //add the directory 'folders' as a root nodes
            foreach (var directory in directoryInfo.GetDirectories())
            {
                directoryNode.Nodes.Add(CreateDirectoryNode(directory));
            }

            //add file names to the child nodes
            foreach (var file in directoryInfo.GetFiles()) 
            { 
                directoryNode.Nodes.Add(new TreeNode(file.Name)); 
            }
                
            return directoryNode;
        }

        #endregion        
    }

    public static class TreeViewIteration
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static IEnumerable<TreeNode> Descendants(this TreeNodeCollection c)
        {
            //return all nodes that are checked
            foreach (var node in c.OfType<TreeNode>())
            {
                yield return node;

                foreach (var child in Descendants(node.Nodes))
                {
                    yield return child;
                }
            }
        }
    }
}
