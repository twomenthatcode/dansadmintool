
Write-Host: "This script will rename the computer and join it to the ULAN Domain"
Write-Host: "."
Write-Host: "."
$computername = Read-Host -Prompt "Enter the desired computer name"
$domain =  "ULAN.local"
$user = Read-Host -Prompt "Enter domain admin user name"
$password = Read-Host -Prompt "Enter password for $user" -AsSecureString
$currentname = get-content env:computername

$username = "$domain\$user"
$credential = New-Object System.Management.Automation.PSCredential($username,$password)
Add-Computer -DomainName $domain -OUPath "OU=ULAN Workstations,DC=ULAN,DC=local" -ComputerName $currentname  -NewName $computername -Credential $credential

Write-Host "Press any key to continue ..."

$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
Exit 