@Echo off

Echo This script will delete the Unattend.xml file,
Echo C:\Drivers, and C:\Temp including the AdminTool.
Echo.
Echo.
SET /P yesno=Delete all install files and storred passwords? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
IF "%yesno%"=="x" GOTO XExitYes
IF "%yesno%"=="X" GOTO XExitYes
GOTO ItemEnd
Echo.
:ItemYes
Echo Prepairing to delete files...
Echo.

Echo Removing Unattend.xml...
del /Q /F c:\windows\system32\sysprep\unattend.xml
del /Q /F c:\windows\panther\unattend.xml

Echo Removing C:\Drivers...
rd /Q /S c:\drivers\

Echo Removing  C:\Temp...
rd /Q /S c:\Temp\
Echo.


Echo Removing  Desktop Icons...
del /Q /F "C:\_AdminTools.lnk"
del /Q /F "C:\_Cleanup.lnk"


Echo All files except for the "cleanup.bat" file have been deleted.
Echo You can delete "cleanup.bat" manually from the C:\ drive.
Pause
GOTO ItemEnd

:ItemEnd
Exit