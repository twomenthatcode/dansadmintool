
REM Runs GPResult and outputs the results to a html file located in C:\Temp with the current date/time


@ECHO OFF
Echo Creating Report...
gpresult /h "c:\temp\GPReport_%date:~10,4%%date:~4,2%%date:~7,2%_%time:~0,2%%time:~3,2%.html"
Echo.
Echo.
Echo The report can be found at c:\temp\GPReport_<Date_Time>.html
Echo.
Echo.
PAUSE
exit