If you are installing this image for the first time then you should have already been prompted to add
this computer to the domain and activate both Windows and Office. The next step will be to restart and log on as 
a domain administrator in order to run "gpupdate /force"

If any of these automated processes failed, you can still run them again from "AdminTools" which can be found on 
the local administrator desktop, or at C:\_AdminTools.lnk

If you would like to perform these functions manually, you can look in C:\Temp for install files, keys and drivers.

Once you have completed the setup process, make sure to run C:\_Cleanup before allowing users to log in. This will
delete all of the setup files, keys, drivers, etc. from C:\Temp as well as the unattend.xml files.