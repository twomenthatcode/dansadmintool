@ECHO OFF
Echo This tool should be ran by using th "Run as administrator" option.
Echo If you have not run this as an administrator, 
Echo please exit and run again.
Echo.
Echo.
Pause

:Start
@ECHO OFF

Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo Admin Tool Options:
Echo 1. Run WASSP Script
Echo 2. Change computer name and add to domain
Echo 3. Activate Windows
Echo 4. Activate MS Office
Echo 5. Add C:\Drivers to registry
Echo 6. Install Symantec Endpoint Protection (also part of #7)
Echo 7. Install all programs to create new Base/Standard Image
Echo 8. Run cleanup script to delete install files and storred passwords
Echo 9. Run Sysprep to create Deployble Image
Echo 10. Shudtown Computer
Echo 11. Create Group Policy Report (Saved to C:\Temp)
Echo 12. WSUS - Expire cookie, initiate detection, update membership.
Echo 13. WSUS - Search for updates. Initiate detection, Query WSUS for updates.
Echo 14. WSUS - Send reporting events. Initiate detection, Query WSUS for updates.
Echo Type X to exit without shutting down computer
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.

Rem ------------------------------------------------------------- Basic / Advanced Setup
SET /P NumberIn=Enter the number associated with the action you wish to perform and press enter [#]:
IF "%NumberIn%"=="1" GOTO WASSP
IF "%NumberIn%"=="2" GOTO DomainAdd
IF "%NumberIn%"=="3" GOTO ActivateWindows
IF "%NumberIn%"=="4" GOTO ActivateMSOffice
IF "%NumberIn%"=="5" GOTO AddDriversRegistry
IF "%NumberIn%"=="6" GOTO SEP
IF "%NumberIn%"=="7" GOTO InstallPrograms
IF "%NumberIn%"=="8" GOTO RunCleanup
IF "%NumberIn%"=="9" GOTO RunSysprep
IF "%NumberIn%"=="10" GOTO ShutdownComputer
IF "%NumberIn%"=="11" GOTO GPReport
IF "%NumberIn%"=="12" GOTO WSUS_Exp
IF "%NumberIn%"=="13" GOTO WSUS_a
IF "%NumberIn%"=="14" GOTO WSUS_r
IF "%NumberIn%"=="X" GOTO XExit
IF "%NumberIn%"=="x" GOTO XExit
GOTO BadInput

:BadInput
Echo Please Select one of the options listed or type X to exit
GOTO Start

Rem ------------------------------------------------------------- WSUS - Expire the cookie, initiate detection, update computer group membership.
:WSUS_Exp
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo  wuauclt - The wuauclt utility allows you some control
Echo            over the Windows Update Agent
Echo.
Echo  WSUS uses a cookie on client computers to store computer group membership
Echo  when client-side targeting is used. This cookie expires an hour after
Echo  WSUS creates it. If you are using client-side targeting and change group
Echo  membership, use /ResetAuthorization /detectnow to expire the cookie, initiate
Echo  detection, and have WSUS update computer group membership.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Run "wuauclt /ResetAuthorization /detectnow" ? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Running "wuauclt /ResetAuthorization /detectnow"...
Echo.
wuauclt /ResetAuthorization /detectnow
GOTO Start

Rem ------------------------------------------------------------- WSUS - Initiate search for updates. Initiate detection, Query the WSUS for updates.
:WSUS_a
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo     wuauclt - The wuauclt utility allows you some control
Echo                   over the Windows Update Agent
Echo.
Echo          /r - Send all queued reporting events to the server asynchronously.
Echo.
Echo /DetectNow - Initiate detection right away, Query the WSUS server  
Echo                     immediately to see if any new updates are needed.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=     Run "wuauclt /r /detectnow" ? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Running "wuauclt /r /detectnow"...
Echo.
wuauclt /r /detectnow
GOTO Start

Rem ------------------------------------------------------------- WSUS - Send all queued reporting events. Initiate detection, Query the WSUS for updates.
:WSUS_r
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo     wuauclt - The wuauclt utility allows you some control
Echo                  over the Windows Update Agent
Echo.
Echo.
Echo          /a - Initiate an asynchronous background search for applicable updates.
Echo                If Automatic Updates are disabled, this has no effect.
Echo.
Echo.
Echo /DetectNow - Initiate detection right away, Query the WSUS server immediately 
Echo                    to see if any new updates are needed.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Run "wuauclt /a /detectnow" ? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Running "wuauclt /a /detectnow"...
Echo.
wuauclt /a /detectnow
GOTO Start

Rem ------------------------------------------------------------- Run GPReport
:GPReport
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Create GP Report? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Creating Group Policy Report (Saved to C:\Temp)...
Echo.
Start /WAIT C:\Temp\Scripts\GPReport.bat
GOTO Start

Rem ------------------------------------------------------------- Run WASSP Script
:WASSP
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Run WASSP Script? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Running WASSP Script...
Echo.
Start /WAIT C:\Temp\Scripts\WASSPfilepermandremove32bit.bat
GOTO Start

Rem ------------------------------------------------------------- Add To Domain
:DomainAdd
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo 1.) ACEnet
Echo 2.) ULAN
Echo 3.) Standalone
Echo X.) Back To Main Menu
Echo.

SET /P DomainNum=Which domain would you like to join? [#]:

IF "%DomainNum%"=="1" GOTO DomainACEnet
IF "%DomainNum%"=="2" GOTO DomainULAN
IF "%DomainNum%"=="3" GOTO DomainStandalone
GOTO Start

:DomainACEnet

Echo Running Domain Add Script...
Echo.
Start /WAIT PowerShell.exe -ExecutionPolicy Unrestricted -File C:\Temp\Scripts\JoinDomainACEnet.ps1 -force
GOTO Start

:DomainULAN
Echo Running Domain Add Script...
Echo.
Start /WAIT PowerShell.exe -ExecutionPolicy Unrestricted -File C:\Temp\Scripts\JoinDomainULAN.ps1 -force
GOTO Start

:DomainStandalone
Echo Running Domain Add Script...
Echo.
Start /WAIT PowerShell.exe -ExecutionPolicy Unrestricted -File C:\Temp\Scripts\Standalone.ps1 -force
GOTO Start

Rem ------------------------------------------------------------- Activate Windows
:ActivateWindows
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Activate Windows? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Running Windows Activation Script...
Echo.
Start /WAIT C:\Temp\Scripts\ActivateWin7.bat
GOTO Start

Rem ------------------------------------------------------------- Activate MS Office
:ActivateMSOffice
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Activate MS Office? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Running MS Office Activation Script...
Echo.
Start /WAIT C:\Temp\Scripts\ActivateMSOffice.bat
GOTO Start

Rem ------------------------------------------------------------- Cleanup Script
:RunCleanup
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Delete install files and storred passwords? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Removing install files and storred passwords...
Echo.
Start /WAIT C:\Cleanup.bat
GOTO Start

Rem ------------------------------------------------------------- Shutdown Computer
:ShutdownComputer
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Shutdown Computer? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Shutting Down Computer...
Echo.
SHUTDOWN -s

Rem ------------------------------------------------------------- Install all for Base Image
:InstallPrograms
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Install all programs to create new Base/Standard Image? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Beginning program installation...
Echo.
Start /WAIT C:\Temp\Scripts\InstallPrograms.bat
GOTO Start
Rem ------------------------------------------------------------- Run Sysprep to create Deployble Image
:RunSysprep
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Run Sysprep to create Deployble Image? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Running Sysprep...
Echo.
Start /WAIT C:\Temp\Scripts\RunSysprep.bat
GOTO Start
Rem ------------------------------------------------------------- Add C:\Drivers to registry
:AddDriversRegistry
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Add C:\Drivers to registry? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Adding C:\Drivers to the registry...
Echo.
Start /WAIT C:\Temp\Scripts\AddDriversReg(wPrompt).bat
GOTO Start

Rem ------------------------------------------------------------- Install Symantec Endpoint Protection
:SEP
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Install Symantec Endpoint Protection? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Echo Installing Symantec Endpoint Protection...
Echo.
Start /WAIT C:\Temp\InstallFiles\SEPpkg.exe
GOTO Start

Rem ------------------------------------------------------------- Exit Admin Tools
:XExit
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
Echo.
SET /P yesno=Exit Admin Tools? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO Start

:ItemYes
Exit