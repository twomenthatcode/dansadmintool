@ECHO OFF
:Start

cls
Echo Automated setup will perform the following:
Echo.
Echo 1. Run WASSP Script
Echo 2. Change computer name and add to domain
Echo 3. Activate Windows
Echo 4. Activate MS Office
Echo.
Echo.
Echo If you would like to go through each step with y/n prompts,
Echo type "N" and press ENTER.
Echo.
Echo If you would like to allow automated setup to run,
Echo type "Y" and press ENTER.
Echo.
Echo.
Echo Type X at any prompt to exit without making any changes.
Echo.
Echo.
Start /WAIT C:\Temp\Scripts\CreateAdminToolsLink.bat
Start /WAIT C:\Temp\Scripts\CreateCleanupLink.bat

Rem ------------------------------------------------------------- Basic / Advanced Setup
Echo.
Echo.
SET /P yesno=Allow automated setup to run? [y/n]:
IF "%yesno%"=="y" GOTO AdvancedSetupNo
IF "%yesno%"=="Y" GOTO AdvancedSetupNo
IF "%yesno%"=="x" GOTO XExit
IF "%yesno%"=="X" GOTO XExit
GOTO AdvancedSetupYes

:AdvancedSetupYes

REM Echo Entering Advanced Setup...

Rem ------------------------------------------------------------- Run WASSP Script
cls
SET /P yesno=Run WASSP Script? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
IF "%yesno%"=="x" GOTO XExitYes
IF "%yesno%"=="X" GOTO XExitYes
GOTO ItemEnd

:ItemYes

Echo Running WASSP Script...
Echo.
Start /WAIT C:\Temp\Scripts\WASSPfilepermandremove32bit.bat
GOTO ItemEnd

:ItemEnd
Rem ------------------------------------------------------------- Add To Domain
cls
SET /P yesno=Add to domain / change computer name? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
IF "%yesno%"=="x" GOTO XExitYes
IF "%yesno%"=="X" GOTO XExitYes
GOTO ItemEnd

:ItemYes
Echo.
Echo.
Echo.
Echo Running Domain Add Script...
Echo.
Echo 1.) ACEnet
Echo 2.) ULAN
Echo 3.) Standalone
Echo.
SET /P DomainNum=Which domain would you like to join? [#]:

IF "%DomainNum%"=="1" GOTO DomainACEnet
IF "%DomainNum%"=="2" GOTO DomainULAN
IF "%DomainNum%"=="3" GOTO DomainStandalone
IF "%DomainNum%"=="x" GOTO XExitYes
IF "%DomainNum%"=="X" GOTO XExitYes
GOTO ItemEnd

:DomainACEnet
cls
Echo Running Domain Add Script...
Echo.
Start /WAIT PowerShell.exe -ExecutionPolicy Unrestricted -File C:\Temp\Scripts\JoinDomainACEnet.ps1 -force
GOTO ItemEnd

:DomainULAN
cls
Echo Running Domain Add Script...
Echo.
Start /WAIT PowerShell.exe -ExecutionPolicy Unrestricted -File C:\Temp\Scripts\JoinDomainULAN.ps1 -force
GOTO ItemEnd

:DomainStandalone
cls
Echo Running Domain Add Script...
Echo.
Start /WAIT PowerShell.exe -ExecutionPolicy Unrestricted -File C:\Temp\Scripts\Standalone.ps1 -force
GOTO ItemEnd

:ItemEnd
Rem ------------------------------------------------------------- Add C:\Drivers to registry
Rem SET /P yesno=Add C:\Drivers to registry? [y/n]:
Rem IF "%yesno%"=="y" GOTO ItemYes
Rem IF "%yesno%"=="Y" GOTO ItemYes
Rem IF "%yesno%"=="x" GOTO XExitYes
Rem IF "%yesno%"=="X" GOTO XExitYes
Rem GOTO ItemEnd

Rem :ItemYes
Rem Start /WAIT C:\Temp\Scripts\DriverPath.reg
Rem GOTO ItemEnd

Rem :ItemEnd
Rem ------------------------------------------------------------- Install Symantec Endpoint Protection
Rem SET /P yesno=Install Symantec Endpoint Protection? [y/n]:
Rem IF "%yesno%"=="y" GOTO ItemYes
Rem IF "%yesno%"=="Y" GOTO ItemYes
Rem IF "%yesno%"=="x" GOTO XExitYes
Rem IF "%yesno%"=="X" GOTO XExitYes
Rem GOTO ItemEnd

Rem :ItemYes
Rem Echo Installing Symantec Endpoint Protection...
Rem Start /WAIT C:\Temp\InstallFiles\SEPpkg.exe
Rem GOTO ItemEnd

Rem :ItemEnd
Rem ------------------------------------------------------------- Activate Windows
cls
SET /P yesno=Activate Windows? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
IF "%yesno%"=="x" GOTO XExitYes
IF "%yesno%"=="X" GOTO XExitYes
GOTO ItemEnd

:ItemYes

Echo Running Windows Activation Script...
Echo.
Start /WAIT C:\Temp\Scripts\ActivateWin7.bat
GOTO ItemEnd

:ItemEnd
Rem ------------------------------------------------------------- Activate MS Office
cls
SET /P yesno=Activate MS Office? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
IF "%yesno%"=="x" GOTO XExitYes
IF "%yesno%"=="X" GOTO XExitYes
GOTO RestartComputerYN

:ItemYes
Echo Running MS Office Activation Script...
Echo.
Start /WAIT C:\Temp\Scripts\ActivateMSOffice.bat
GOTO RestartComputerYN

Rem ------------------------------------------------------------- Advanced Setup = No
:AdvancedSetupNo
cls
Echo Running Automated Setup:
cls
Echo Running WASSP Script...
Start /WAIT C:\Temp\Scripts\WASSPfilepermandremove32bit.bat
cls
Echo Running Domain Add Script...
Echo.
Echo 1.) ACEnet
Echo 2.) ULAN
Echo 3.) Standalone
Echo.
SET /P DomainNum=Which domain would you like to join? [#]:

IF "%DomainNum%"=="1" GOTO DomainACEnet
IF "%DomainNum%"=="2" GOTO DomainULAN
IF "%DomainNum%"=="3" GOTO DomainStandalone
IF "%DomainNum%"=="x" GOTO XExitYes
IF "%DomainNum%"=="X" GOTO XExitYes
GOTO ItemEnd

:DomainACEnet
cls
Echo Running Domain Add Script...
Echo.
Start /WAIT PowerShell.exe -ExecutionPolicy Unrestricted -File C:\Temp\Scripts\JoinDomainACEnet.ps1 -force
GOTO ItemEnd

:DomainULAN
cls
Echo Running Domain Add Script...
Echo.
Start /WAIT PowerShell.exe -ExecutionPolicy Unrestricted -File C:\Temp\Scripts\JoinDomainULAN.ps1 -force
GOTO ItemEnd

:DomainStandalone
cls
Echo Running Domain Add Script...
Echo.
Start /WAIT PowerShell.exe -ExecutionPolicy Unrestricted -File C:\Temp\Scripts\Standalone.ps1 -force
GOTO ItemEnd

:ItemEnd
rem Echo Installing Symantec Endpoint Protection...
rem Start /WAIT C:\Temp\InstallFiles\SEPpkg.exe
Echo.
Echo.
Echo Running Windows Activation Script...
Start /WAIT C:\Temp\Scripts\ActivateWin7.bat
Echo.
Echo.
Echo Running MS Office Activation Script...
Start /WAIT C:\Temp\Scripts\ActivateMSOffice.bat
Echo.
Echo.
Echo Automated Setup Complete.
Echo.
Echo.
PAUSE
GoTo RestartComputerYN
Rem ------------------------------------------------------------- Cleanup Script
Rem :RunCleanup
Rem SET /P yesno=Delete all install files, scripts and storred passwords? [y/n]:
Rem IF "%yesno%"=="y" GOTO ItemYes
Rem IF "%yesno%"=="Y" GOTO ItemYes
Rem IF "%yesno%"=="x" GOTO XExitYes
Rem IF "%yesno%"=="X" GOTO XExitYes
Rem GOTO :RestartComputerYN

Rem :ItemYes
Rem Start /WAIT C:\Cleanup.bat
Rem GOTO RestartComputerYN

Rem ------------------------------------------------------------- Exit
:XExit
cls
Echo Be Sure to run the cleanup script found on the desktop of 
Echo the local administrator account before allowing users to 
Echo log in.
Echo.
Echo The cleanup script will delete all unused reasources, scripts, 
Echo drivers, and storred passwords.
Echo.
Echo If you need to rerun any of the scripts provided, 
Echo use AdminTools.bat found in C:/Temp.
Echo.
Echo.
Echo.
SET /P yesno=To exit type X and press enter. To start from the beginning press any other key and press enter:
IF "%yesno%"=="x" GOTO XExitYes
IF "%yesno%"=="X" GOTO XExitYes
GOTO Start

:XExitYes
Exit

Rem ------------------------------------------------------------- Restart Computer
:RestartComputerYN
cls
Echo Be Sure to run the cleanup script found on the desktop of 
Echo the local administrator account before allowing users to 
Echo log in.
Echo.
Echo The cleanup script will delete all unused reasources, scripts, 
Echo drivers, and storred passwords.
Echo.
Echo If you need to rerun any of the scripts provided, 
Echo use AdminTools.bat found in C:/Temp.
Echo.
Echo.
Echo.
SET /P yesno=Restart Computer? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
IF "%yesno%"=="x" GOTO XExitYes
IF "%yesno%"=="X" GOTO XExitYes
GOTO XExit

:ItemYes
cls
Echo System is restarting...
SHUTDOWN -r
Echo.
Echo.
Echo Please wait for the countdown timer to finish before 
Echo performing any other action.
Echo.
TIMEOUT /T 300
cls
Echo If the system has not restarted, please reboot manually.
Echo To exit without rebooting type "x" and press ENTER
PAUSE


