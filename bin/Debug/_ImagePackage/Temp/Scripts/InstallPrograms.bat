@ECHO OFF
Echo Type X at any prompt to exit
Echo.
:start
Echo Base Image includes:
Echo 1.) .NET 4.5 - Needed for Join Domain Scripts
Echo 2.) PowerShell 4.0 - Needed for Join Domain Scripts
Echo 2.) Symantec Endpoint Protection
Echo.

SET /P yesno=Install all for Base Image? [y/n]:
IF "%yesno%"=="y" GOTO InstallYes
IF "%yesno%"=="Y" GOTO InstallYes
IF "%yesno%"=="x" GOTO InstallEnd
IF "%yesno%"=="X" GOTO InstallEnd
GOTO Standard

:InstallYes
Echo Installing .NET 4.5...
Start /WAIT C:\Temp\InstallFiles\NDP451-KB2858728-x86-x64-AllOS-ENU.exe
Echo Installing PowerShell 4.0...
Start /WAIT C:\Temp\InstallFiles\Windows6.1-KB2819745-x86-MultiPkg.msu
Echo Installing Symantec Endpoint Protection...
Start /WAIT C:\Temp\InstallFiles\SEPpkg.exe
GOTO Standard
Echo.
Echo.
Echo.
Echo.
:Standard
Echo Standard Image includes:
Echo 1.) MS Office
Echo.
Echo.
Echo.

SET /P yesno=Install all for Standard Image? [y/n]:
IF "%yesno%"=="y" GOTO InstallYes
IF "%yesno%"=="Y" GOTO InstallYes
IF "%yesno%"=="x" GOTO InstallEnd
IF "%yesno%"=="X" GOTO InstallEnd
GOTO InstallEnd

:InstallYes
Echo Installing MS Office...
Start C:\Temp\InstallFiles\Office2010\Office201032-bit\setup.exe
GOTO InstallEnd

:InstallEnd
Echo.
Echo.
SET /P yesno=To exit type X and press enter, to continue press any other key and press enter:
IF "%yesno%"=="x" GOTO XExitYes
IF "%yesno%"=="X" GOTO XExitYes
GOTO Start

:XExitYes
Exit