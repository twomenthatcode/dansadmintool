Rem Checks is files were deleted
@Echo OFF

Call :ReportDelete "C:\File1.txt"
Call :ReportDelete "C:\File2.txt"
Call :ReportDelete "C:\File3.txt"
Call :ReportDelete "C:\File4.txt"
Call :ReportDelete "C:\File5.txt"

Pause&Exit

:ReportDelete
(Del "%~1" 2>NUL & If exist "%~1" (Echo [+] File failed to delete: "%~1" >> "Report.txt")) & (Goto:EOF)