
echo "This script will rename the computer and join it to the ACEnet.local Domain"
echo "."
echo "."
$computername = Read-Host -Prompt "Enter the desired computer name"
$domain =  "ACEnet.local"
$user = Read-Host -Prompt "Enter domain admin user name"
$password = Read-Host -Prompt "Enter password for $user" -AsSecureString
$currentname = get-content env:computername

$username = "$domain\$user"
$credential = New-Object System.Management.Automation.PSCredential($username,$password)
Add-Computer -DomainName $domain -OUPath "OU=Thick,OU=Workstations,OU=ACENET,DC=ACENET,DC=local" -ComputerName $currentname  -NewName $computername -Credential $credential

echo "Press any key to continue ..."

$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
Exit 