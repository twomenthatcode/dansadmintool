@Echo off

set append=;C:\Drivers
set key=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion
set value=DevicePath
set oldVal=

for /F "skip=2 tokens=3" %%r in ('reg query %key% /v %value%') do set oldVal=%%r
echo previous=%oldVal%

set newVal=%oldVal%%append% 

echo append: %append%
echo to key: %key% "\" %value%
echo old value:%oldVal%
echo new value:%oldVal%%append%

SET /P yesno=Add C:\Drivers to registry? [y/n]:
IF "%yesno%"=="y" GOTO ItemYes
IF "%yesno%"=="Y" GOTO ItemYes
GOTO ItemEnd

:ItemYes
reg add %key% /v %value% /d %newVal% /f
GOTO Start

:ItemEnd
Pause
Exit



