
Write-Host: "This script will rename the computer without changing the domain information"
Write-Host: "."
Write-Host: "."
$computername = Read-Host -Prompt "Enter the desired computer name"
$user = Read-Host -Prompt "Enter local admin user name"
$password = Read-Host -Prompt "Enter password for $user" -AsSecureString
$currentname = get-content env:computername

$username = "$domain\$user"
$credential = New-Object System.Management.Automation.PSCredential($username,$password)
Rename-Computer -ComputerName $currentname  -NewName $computername -LocalCredential $credential

Write-Host "Press any key to continue ..."

$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
Exit 