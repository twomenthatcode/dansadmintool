@Echo off

set append=;C:\Drivers
set key=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion
set value=DevicePath
set oldVal=

for /F "skip=2 tokens=3" %%r in ('reg query %key% /v %value%') do set oldVal=%%r
echo previous=%oldVal%

set newVal=%oldVal%%append% 

rem echo append: %append%
rem echo to key: %key%
rem echo value: %value%
rem echo old value:%oldVal%
rem echo new value:%oldVal%%append%

reg add %key% /v %value% /d %newVal% /f

rem Pause
Exit



