﻿namespace DansScriptMngr
{
    partial class ctlImgBuilder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tvTest = new System.Windows.Forms.TreeView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.fbdNewDir = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // tvTest
            // 
            this.tvTest.Location = new System.Drawing.Point(3, 0);
            this.tvTest.Name = "tvTest";
            this.tvTest.Size = new System.Drawing.Size(471, 485);
            this.tvTest.TabIndex = 0;
            this.tvTest.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvTest_AfterCheck);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(623, 245);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(92, 23);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Add To Image";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // fbdNewDir
            // 
            this.fbdNewDir.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // ctlImgBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tvTest);
            this.Name = "ctlImgBuilder";
            this.Size = new System.Drawing.Size(822, 491);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView tvTest;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.FolderBrowserDialog fbdNewDir;
    }
}
