﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.XPath;    

namespace DansScriptMngr
{
    public partial class ctlContacts : UserControl
    {
        string _rootPath = string.Empty;
       
        public ctlContacts()
        {
            InitializeComponent();

            _rootPath = System.IO.Directory.GetCurrentDirectory();

            XmlDocument docXMLInternal = new XmlDocument();
            // Load the xml file
            docXMLInternal.Load(_rootPath + @"\_Xml\internalContacts.xml");

            // Populate all of the base nodes
            populateBaseNodes(docXMLInternal, treeView1);

            XmlDocument docXMLExternal = new XmlDocument();
            // Load the xml file
            docXMLExternal.Load(_rootPath + @"\_Xml\externalContacts.xml");

            // Populate all of the base nodes
            populateBaseNodes(docXMLExternal, treeView2); 
        }

        private void populateBaseNodes(XmlDocument docXML, TreeView tv)
        {
            // Clear any existing items
            tv.Nodes.Clear();

            // Begin updating the treeview
            tv.BeginUpdate();
            TreeNode treenode;
            treenode = tv.Nodes.Add("Contacts");

            // Get all first level <contact> nodes
            XmlNodeList baseNodeList = docXML.SelectNodes("root/contact");
           

            foreach (XmlNode xmlnode in baseNodeList)
            // loop through all base <folder> nodes 
            {
                string title = xmlnode.Attributes["title"].Value;

                treenode = tv.Nodes.Add(title); // add it to the tree

                populateChildNodes(xmlnode, treenode); // Get the children
            }

            tv.EndUpdate(); // Stop updating the tree
            tv.Refresh(); // refresh the treeview display
        }

        private void populateChildNodes(XmlNode oldXmlnode, TreeNode oldTreenode)
        {
            TreeNode treenode = null;
            XmlNodeList childNodeList = oldXmlnode.ChildNodes;
            // Get all children for the past node (parent)

            foreach (XmlNode xmlnode in childNodeList)
            // loop through all children
            {
                string title = xmlnode.Attributes["title"].Value;
                // add it to the parent node tree
                treenode = oldTreenode.Nodes.Add(title);
                populateChildNodes(xmlnode, treenode);
            }
        }
    }
}
