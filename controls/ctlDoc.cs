﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DansScriptMngr.classes;
using System.Diagnostics;


namespace DansScriptMngr
{
    public partial class ctlDoc : UserControl
    {
        string _rootPath = string.Empty;

        public ctlDoc()
        {
            InitializeComponent();

            _rootPath = System.IO.Directory.GetCurrentDirectory();
            
            string scriptPath = _rootPath + @"\_Docs";
            GenClass.ListDirectory(treeView1, scriptPath + @"\SOP");
            GenClass.ListDirectory(treeView2, scriptPath + @"\Forms");
        }


        private void treeView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            TreeNode node = treeView1.SelectedNode;
            string nodePath = _rootPath + @"\_Docs\" + node.FullPath;

            Process.Start(nodePath);

            //MessageBox.Show(string.Format("You selected: {0}\n\n root Path {1}", node.Text, _rootPath));
        }

        private void treeView2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            TreeNode node = treeView2.SelectedNode;
            string nodePath = _rootPath + @"\_Docs\" + node.FullPath;

            Process.Start(nodePath);

            //MessageBox.Show(string.Format("You selected: {0}\n\n root Path {1}", node.Text, _rootPath));
        }
    }
}