﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DansScriptMngr.classes;
namespace DansScriptMngr
{
    public partial class ctlScripts : UserControl
    {
        string _rootPath = string.Empty;

        public ctlScripts()
        {
            InitializeComponent();

            _rootPath = System.IO.Directory.GetCurrentDirectory();

            string scriptPath = _rootPath + @"\_Scripts";
            GenClass.ListDirectory(tvTest, scriptPath);
        }


        private void tvTest_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            TreeNode node = tvTest.SelectedNode;
            string nodePath = node.FullPath;

            System.Diagnostics.Process.Start(_rootPath + "\\" + nodePath);

            //MessageBox.Show(string.Format("You selected: {0}\n\n root Path {1}", node.Text, _rootPath));
        }
    }
}
