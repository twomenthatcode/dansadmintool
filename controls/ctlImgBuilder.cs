﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DansScriptMngr.classes;



namespace DansScriptMngr
{
    public partial class ctlImgBuilder : UserControl
    {
        public class FileObj
        {
            public string OrgPath { get; set; }
            public string NewPath { get; set; }
            public string FileName { get; set; }
            public string OrgDir { get; set; }
            public string NewDir { get; set; }
        }

        string _rootPath = string.Empty;

        public ctlImgBuilder()
        {
            InitializeComponent();

            //get the root path of project
            _rootPath = System.IO.Directory.GetCurrentDirectory();

            //set the path to add the directory to the TreeView
            string scriptPath = _rootPath + @"\_ImagePackage";
            
            //add nodes to TreeView
            GenClass.ListDirectory(tvTest, scriptPath);

            //enable check boxes on TreeView
            tvTest.CheckBoxes = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tvTest_AfterCheck(object sender, TreeViewEventArgs e)
        {
            bool busy = false;
                        
            if (busy) return;

            busy = true;
            try {
                // check all child nodes under a parent node
                checkNodes(e.Node, e.Node.Checked);
            }
            finally {
                busy = false;
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="check"></param>
        private void checkNodes(TreeNode node, bool check)
        {
            //check all child nodes under a parent node
            foreach (TreeNode child in node.Nodes)
            {
                child.Checked = check;
                checkNodes(child, check);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //root directory for new destination
            string targetDir = string.Empty;

            DialogResult result = fbdNewDir.ShowDialog();
            if (result == DialogResult.OK)
            {
                targetDir = fbdNewDir.SelectedPath;
            }
            if (result == DialogResult.Cancel)
            {
                return;
            }


            List<FileObj> retList = new List<FileObj>();
            FileObj ret;

            //LINQ expression to get all of the checked nodes in the tree
            var chkd = (from a in TreeViewIteration.Descendants(tvTest.Nodes)
                        where a.Checked == true
                        select new { a.FullPath }).ToList();

            foreach (var x in chkd)
            {
                ret = new FileObj();

                ret.OrgPath = _rootPath + "\\" + x.FullPath;
                ret.NewPath = targetDir + "\\" + x.FullPath;
                ret.FileName = Path.GetFileName(x.FullPath);
                ret.OrgDir = _rootPath + "\\" + Path.GetDirectoryName(x.FullPath);
                ret.NewDir = targetDir + "\\" + Path.GetDirectoryName(x.FullPath);

                retList.Add(ret);
            }

            foreach (var i in retList)
            {
                if (!Directory.Exists(i.NewDir))
                {
                    Directory.CreateDirectory(i.NewDir);
                }

                if (File.Exists(i.OrgPath))
                {
                    File.Copy(i.OrgPath, i.NewPath, true);
                }
            }

        }

        
       
    }
    
}

