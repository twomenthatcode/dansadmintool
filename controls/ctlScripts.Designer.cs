﻿namespace DansScriptMngr
{
    partial class ctlScripts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("ActivateMSOffice.bat");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("ActivateWin7.bat");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Activation Scripts", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("CreateAdminToolsLink.bat");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("CreateCleanupLink.bat");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Create Link", new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("RunSysprep.bat");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("RunFirst.bat");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("InstallPrograms.bat");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("WASSPfilepermandremove32bit.bat");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Cleanup.bat");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("SetupComplete.cmd");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Image Scripts", new System.Windows.Forms.TreeNode[] {
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12});
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("JoinDomainACEnet.ps1");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("JoinDomainULAN.ps1");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("Standalone.ps1");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Join Domain Scripts", new System.Windows.Forms.TreeNode[] {
            treeNode14,
            treeNode15,
            treeNode16});
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("CMDpermissionsAdministrator.bat");
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Permissioning", new System.Windows.Forms.TreeNode[] {
            treeNode18});
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("AddDriversReg(wPrompt).bat");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("Registry Edits", new System.Windows.Forms.TreeNode[] {
            treeNode20});
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("GPReport.bat");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("Reports", new System.Windows.Forms.TreeNode[] {
            treeNode22});
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("Scripts", new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode6,
            treeNode13,
            treeNode17,
            treeNode19,
            treeNode21,
            treeNode23});
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("Test.bat");
            this.tvTest = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // tvTest
            // 
            this.tvTest.Location = new System.Drawing.Point(3, 3);
            this.tvTest.Name = "tvTest";
            treeNode1.Name = "Node1";
            treeNode1.Text = "ActivateMSOffice.bat";
            treeNode1.ToolTipText = "Activates MS Office on computers with an internet connection";
            treeNode2.Name = "Node2";
            treeNode2.Text = "ActivateWin7.bat";
            treeNode2.ToolTipText = "Activates Windows 7 on computers with an internet connection";
            treeNode3.Name = "Node18";
            treeNode3.Text = "Activation Scripts";
            treeNode4.Name = "Node7";
            treeNode4.Text = "CreateAdminToolsLink.bat";
            treeNode5.Name = "Node8";
            treeNode5.Text = "CreateCleanupLink.bat";
            treeNode6.Name = "Node20";
            treeNode6.Text = "Create Link";
            treeNode7.Name = "Node14";
            treeNode7.Text = "RunSysprep.bat";
            treeNode7.ToolTipText = "Runs Sysprep on the current machine, then shuts down the computer.";
            treeNode8.Name = "Node13";
            treeNode8.Text = "RunFirst.bat";
            treeNode8.ToolTipText = "Runs basic automated functions for image at first start.";
            treeNode9.Name = "Node10";
            treeNode9.Text = "InstallPrograms.bat";
            treeNode9.ToolTipText = "Installs common programs onto the image through a simple prompt.";
            treeNode10.Name = "Node17";
            treeNode10.Text = "WASSPfilepermandremove32bit.bat";
            treeNode10.ToolTipText = "Removes unwanted files from the image.";
            treeNode11.Name = "Node5";
            treeNode11.Text = "Cleanup.bat";
            treeNode11.ToolTipText = "This script will delete the Unattend.xml file,C:\\Drivers, and C:\\Temp including t" +
    "he AdminTool.";
            treeNode12.Name = "Node25";
            treeNode12.Text = "SetupComplete.cmd";
            treeNode12.ToolTipText = "Will run automatically after first start of OS. Currently configured to run the \"" +
    "RunFirst.bat\" script. This file should be places in \"C:\\Windows\\Setup\\Scripts\" f" +
    "older. ";
            treeNode13.Name = "Node22";
            treeNode13.Text = "Image Scripts";
            treeNode14.Name = "Node11";
            treeNode14.Text = "JoinDomainACEnet.ps1";
            treeNode14.ToolTipText = "Joins the current computer to the ACENet domain";
            treeNode15.Name = "Node12";
            treeNode15.Text = "JoinDomainULAN.ps1";
            treeNode15.ToolTipText = "Joins the current computer to the ULAN domain";
            treeNode16.Name = "Node15";
            treeNode16.Text = "Standalone.ps1";
            treeNode16.ToolTipText = "Renames the computer for a standalone image";
            treeNode17.Name = "Node21";
            treeNode17.Text = "Join Domain Scripts";
            treeNode18.Name = "Node6";
            treeNode18.Text = "CMDpermissionsAdministrator.bat";
            treeNode18.ToolTipText = "grants the built in administrator full contol to CMD.exe";
            treeNode19.Name = "Node24";
            treeNode19.Text = "Permissioning";
            treeNode20.Name = "Node3";
            treeNode20.Text = "AddDriversReg(wPrompt).bat";
            treeNode20.ToolTipText = "Edits the registry to include \"C:\\Drivers\\\"";
            treeNode21.Name = "Node19";
            treeNode21.Text = "Registry Edits";
            treeNode22.Name = "Node9";
            treeNode22.Text = "GPReport.bat";
            treeNode22.ToolTipText = "Creates a group policy report";
            treeNode23.Name = "Node23";
            treeNode23.Text = "Reports";
            treeNode24.Name = "Node0";
            treeNode24.Text = "Scripts";
            treeNode25.Name = "Node16";
            treeNode25.Text = "Test.bat";
            treeNode25.ToolTipText = "Used to test the use of batch files.";
            this.tvTest.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode24,
            treeNode25});
            this.tvTest.Size = new System.Drawing.Size(822, 491);
            this.tvTest.TabIndex = 0;
            this.tvTest.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.tvTest_MouseDoubleClick);
            // 
            // ctlScripts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.tvTest);
            this.Name = "ctlScripts";
            this.Size = new System.Drawing.Size(828, 497);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView tvTest;

    }
}
