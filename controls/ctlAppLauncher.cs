﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace DansScriptMngr
{
    public partial class ctlAppLauncher : UserControl
    {
        string _rootPath = string.Empty;

        public ctlAppLauncher()
        {
            InitializeComponent();

            _rootPath = System.IO.Directory.GetCurrentDirectory();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ExecutableFilePath = _rootPath + @"\_Resources\Ghost 32\GHOST32.EXE";
            string Arguments = @"";

            if (File.Exists(ExecutableFilePath))
            {
                System.Diagnostics.Process.Start(ExecutableFilePath, Arguments);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string ExecutableFilePath = @"C:\Windows\System32\mmc.exe";
            string Arguments = @"";

            if (File.Exists(ExecutableFilePath))
            {
                System.Diagnostics.Process.Start(ExecutableFilePath, Arguments);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string ExecutableFilePath = @"C:\Windows\System32\taskmgr.exe";
            string Arguments = @"";

            if (File.Exists(ExecutableFilePath))
            {
                System.Diagnostics.Process.Start(ExecutableFilePath, Arguments);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string ExecutableFilePath = @"C:\Windows\System32\cmd.exe";
            string Arguments = @"";

            if (File.Exists(ExecutableFilePath))
            {
                System.Diagnostics.Process.Start(ExecutableFilePath, Arguments);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string ExecutableFilePath = @"C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe";
            string Arguments = @"";

            if (File.Exists(ExecutableFilePath))
            {
                System.Diagnostics.Process.Start(ExecutableFilePath, Arguments);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string ExecutableFilePath = @"C:\Windows\System32\regedit.exe";
            string Arguments = @"";

            if (File.Exists(ExecutableFilePath))
            {
                System.Diagnostics.Process.Start(ExecutableFilePath, Arguments);
            }
        }
    }
}
